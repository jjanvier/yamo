<?php

namespace Jjanvier\YamoExamples;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Http\Message\ServerRequestInterface;

class CacheMiddleware implements MiddlewareInterface
{
    /** @var CacheItemPoolInterface */
    private $cache;

    public function __construct(CacheItemPoolInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * {@inheritdoc}
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $key = $request->getUri()->getPath();
        $key = str_replace('/', '#', $key); // "/" can not be used inside the cache key
        $item = $this->cache->getItem($key);

        if ($item->isHit()) {
            $cachedResponse = $item->get();
        } else {
            $response = $delegate->process($request);
            $item->set($response);
            $this->cache->save($item);
            $cachedResponse = $response;
        }

        return $cachedResponse;
    }
}
