<?php

namespace Jjanvier\YamoExamples;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;

/**
 * A middleware that authenticates the user with basic HTTP.
 * Needs to be authenticated with "foo"/"bar".
 *
 * Request is not altered.
 *
 * If user is not authenticated, a default 403 response is returned.
 * Otherwise, response is not altered.
 */
class AuthMiddleware implements MiddlewareInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $params = $request->getServerParams();
        if (isset($params['PHP_AUTH_USER']) && isset($params['PHP_AUTH_PW']) &&
            'foo' === $params['PHP_AUTH_USER'] && 'bar' === $params['PHP_AUTH_PW']
        ) {
            return $delegate->process($request);
        }

        return new HtmlResponse('You can\'t access the Yamo realm!', 403);
    }
}
