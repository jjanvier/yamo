<?php

namespace Jjanvier\YamoExamples;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;

/**
 * A middleware to catch exception raised by the others middlewares.
 */
class ErrorMiddleware implements MiddlewareInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        try {
            return $delegate->process($request);
        } catch (\Exception $e) {
            return new HtmlResponse('Snap! An error occurred! ' . $e->getMessage(), 400);
        }
    }
}
