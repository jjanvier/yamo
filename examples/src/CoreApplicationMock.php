<?php

namespace Jjanvier\YamoExamples;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;

/**
 * A mock of an application that just return a simple HTML response.
 */
class CoreApplicationMock implements MiddlewareInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $response = new HtmlResponse('Here is a response returned by the core of our application.');

        return $response;
    }
}
