<?php

use DI\ContainerBuilder;
use Interop\Http\Factory\ServerRequestFactoryInterface;
use Jjanvier\Yamo\Orchestrator;
use Jjanvier\YamoExamples\CoreApplicationMock;
use Jjanvier\YamoExamples\AuthMiddleware;
use Jjanvier\YamoExamples\CacheMiddleware;
use Jjanvier\YamoExamples\ErrorMiddleware;
use Jjanvier\YamoExamples\LoggerMiddleware;

ini_set('xdebug.var_display_max_depth', 7);

require __DIR__ . '/../../vendor/autoload.php';

$containerBuilder = new ContainerBuilder();
$containerBuilder->useAnnotations(false);
$containerBuilder->useAutowiring(true);
$containerBuilder->addDefinitions('php-di-config.php');
$container = $containerBuilder->build();

if (!isset($_SERVER['PHP_AUTH_USER'])) {
    header('WWW-Authenticate: Basic realm="Yamo Realm"');
    header('HTTP/1.0 401 Unauthorized');
}

$serverRequestFactory = $container->get(ServerRequestFactoryInterface::class);

$request = $serverRequestFactory->createServerRequestFromArray($_SERVER);

$orchestrator = new Orchestrator([
    $container->get(ErrorMiddleware::class),
    $container->get(LoggerMiddleware::class),
    $container->get(AuthMiddleware::class),
    $container->get(CacheMiddleware::class),
    $container->get(CoreApplicationMock::class),
]);

$response = $orchestrator->process($request);
echo $response->getBody();
