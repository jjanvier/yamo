<?php

namespace Jjanvier\YamoExamples;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;

/**
 * A middleware that logs the incoming request.
 * Request is not altered.
 * Response is not altered.
 */
class LoggerMiddleware implements MiddlewareInterface
{
    /** @var LoggerInterface */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $params = $request->getServerParams();
        $this->logger->info(
            sprintf(
                'Receiving a "%s" request from the agent "%s".',
                $params['REQUEST_METHOD'],
                $params['HTTP_USER_AGENT']
            )
        );

        return $delegate->process($request);
    }
}
