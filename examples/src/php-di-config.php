<?php

use Http\Factory\Diactoros\ResponseFactory;
use Http\Factory\Diactoros\ServerRequestFactory;
use Interop\Http\Factory\ResponseFactoryInterface;
use Interop\Http\Factory\ServerRequestFactoryInterface;
use Katzgrau\KLogger\Logger;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Component\Cache\Adapter\ArrayAdapter;

return [
    'logger.dir' => sys_get_temp_dir(),
    'logger.level' => LogLevel::DEBUG,
    'logger.options' => ['prefix' => 'yamo_'],
    LoggerInterface::class => DI\object(Logger::class)->constructor(
        DI\get('logger.dir'),
        DI\get('logger.level'),
        DI\get('logger.options')
    ),
    ResponseFactoryInterface::class => DI\object(ResponseFactory::class),
    ServerRequestFactoryInterface::class => DI\object(ServerRequestFactory::class),
    CacheItemPoolInterface::class => DI\object(ArrayAdapter::class)
];
