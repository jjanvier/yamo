<?php

namespace spec\Jjanvier\Yamo;

use Interop\Http\Factory\ResponseFactoryInterface;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Jjanvier\Yamo\Orchestrator;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class OrchestratorSpec extends ObjectBehavior
{
    function it_is_initializable(MiddlewareInterface $cache, MiddlewareInterface $log)
    {
        $this->beConstructedWith([$cache, $log]);
        $this->shouldHaveType(Orchestrator::class);
    }

    function it_is_a_psr15_delegator(MiddlewareInterface $cache, MiddlewareInterface $log)
    {
        $this->beConstructedWith([$cache, $log]);
        $this->shouldImplement(DelegateInterface::class);
    }

    function it_throws_an_exception_when_built_without_middlewares()
    {
        $this->shouldThrow(\InvalidArgumentException::class)->during('__construct', [[]]);
    }

    function it_throws_an_exception_when_built_with_non_middlewares(MiddlewareInterface $foo, \stdClass $bar)
    {
        $this->shouldThrow(\InvalidArgumentException::class)->during('__construct', [[$foo, $bar]]);
    }

    function it_processes_a_request_with_only_one_middleware(
        MiddlewareInterface $cache,
        ServerRequestInterface $request,
        ResponseInterface $response
    ) {
        $this->beConstructedWith([$cache]);

        $cache->process($request, Argument::type(Orchestrator::class))->willReturn($response);

        $this->process($request)->shouldReturn($response);
    }

    function it_processes_a_request_with_the_current_middleware(
        MiddlewareInterface $auth,
        MiddlewareInterface $cache,
        ServerRequestInterface $request,
        ResponseInterface $responseAuth
    ) {
        $this->beConstructedWith([$auth, $cache]);

        $auth->process($request, Argument::type(Orchestrator::class))->willReturn($responseAuth);
        $cache->process(Argument::cetera())->shouldNotBeCalled();

        $this->process($request)->shouldReturn($responseAuth);
    }

    function it_returns_a_default_response_when_no_middleware_did_it(
        ResponseFactoryInterface $responseFactory,
        MiddlewareInterface $auth,
        ServerRequestInterface $request,
        ResponseInterface $response
    ) {
        $this->beConstructedWith([$auth], $responseFactory);

        $responseFactory->createResponse()->willReturn($response);
        $this->process($request)->shouldReturn($response);
    }
}
