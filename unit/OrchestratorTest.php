<?php

namespace unit\Jjanvier\Yamo;

use Http\Factory\Diactoros\ResponseFactory;
use Http\Factory\Diactoros\ServerRequestFactory;
use Http\Factory\Diactoros\StreamFactory;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Jjanvier\Yamo\Orchestrator;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;

class OrchestratorTest extends TestCase
{
    public function testProcessRequestThroughSeveralMiddlewares()
    {
        $requestFactory = new ServerRequestFactory();
        $request = $requestFactory->createServerRequestFromArray([]);

        $first = new FirstMiddleware();
        $second = new SecondMiddleware();
        $coreApp = new CoreApplication();

        $orchestrator = new Orchestrator([$first, $second, $coreApp]);
        $actualResponse = $orchestrator->process($request);

        $this->assertSame(599, $actualResponse->getStatusCode());
        $this->assertCount(2, $actualResponse->getHeaders());
        $this->assertSame(['bar'], $actualResponse->getHeader('second'));
        $this->assertSame(['foo'], $actualResponse->getHeader('first'));
        $this->assertSame(file_get_contents(__FILE__), $actualResponse->getBody()->getContents());
    }

    public function testProcessRequestWithDefaultResponseReturned()
    {
        $requestFactory = new ServerRequestFactory();
        $request = $requestFactory->createServerRequestFromArray([]);

        $first = new FirstMiddleware();
        $second = new SecondMiddleware();

        $orchestrator = new Orchestrator([$first, $second]);
        $actualResponse = $orchestrator->process($request);

        $this->assertSame(200, $actualResponse->getStatusCode());
        $this->assertCount(2, $actualResponse->getHeaders());
        $this->assertSame(['bar'], $actualResponse->getHeader('second'));
        $this->assertSame(['foo'], $actualResponse->getHeader('first'));
        $this->assertSame('', $actualResponse->getBody()->getContents());
    }
}

class FirstMiddleware implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $firstRequest = $request->withAttribute('first', true);
        $response = $delegate->process($firstRequest);
        $firstResponse = $response->withHeader('first', 'foo');

        return $firstResponse;
    }
}

class SecondMiddleware implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $secondRequest = $request->withAttribute('second', true);
        $response = $delegate->process($secondRequest);
        $secondResponse = $response->withHeader('second', 'bar');

        return $secondResponse;
    }
}

class CoreApplication implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $responseFactory = new ResponseFactory();
        $streamFactory = new StreamFactory();

        $response = $responseFactory->createResponse(599);
        $body = $streamFactory->createStreamFromFile(__FILE__);

        return $response->withBody($body);
    }
}
