# yamo

[![Build Status](https://gitlab.com/jjanvier/yamo/badges/master/build.svg)](https://gitlab.com/jjanvier/yamo/commits/master)
[![license](https://img.shields.io/github/license/jjanvier/yamo.svg)](https://opensource.org/licenses/gpl-3.0.html)

Yamo is yet another PSR-15 middleware orchestrator.

---

The purpose of this library is just to help discovering and understanding the [PSR-15](https://github.com/php-fig/fig-standards/blob/master/proposed/http-middleware) about HTTP middlewares.


## Usage

Just provide the list of the middlewares you want to use:

```php
$orchestrator = new Orchestrator([
        // a list of Psr\Http\ServerMiddleware\MiddlewareInterface middlewares
    ]
);

$response = $orchestrator->process($request);

```

Please note that middlewares are called exactly in the order they are defined. For instance
```php
$orchestrator = new Orchestrator([
        $middleware1,
        $middleware2,
        $middleware3,
    ]
);

$response = $orchestrator->process($request);

```
will lead to the following execution flow:

![PSR-15 middlewares flow](examples/PSR-15_middlewares_flow.png)

Each middleware can, if needed, enrich the incoming request until it reaches the core application. 
The core application then processes the request to generate a response. 
This response is transmitted to each middleware which can also, if needed, enrich the response.
Finally, when all middlewares have been reached, the orchestrator returns the final response.

Alternatively, you can also define the [PSR-17](https://github.com/php-fig/fig-standards/tree/master/proposed/http-factory) response factory factory you want to use. This factory will be used
to return a default 200 response in case no middleware provided one. The default response factory is `Http\Factory\Diactoros\ResponseFactory`.

```php
$customResponseFactory = new Http\Factory\Guzzle\ResponseFactory; // any Psr\Http\Message\ResponseFactoryInterface 
$orchestrator = new Orchestrator([
        // a list of Psr\Http\ServerMiddleware\MiddlewareInterface middlewares
    ], 
    $customResponseFactory
);

$response = $orchestrator->process($request);

```

## Examples

For "academic" purpose, some middleware examples are provided in the [examples](examples/src) folder. Those classes are not meant to be used. They can be considered as "advanced pseudo code" to help understand the reader.
The example application can be launched with `php -S 127.0.0.1:8080 -t examples/src/`.

Real PSR-15 middlewares can be found in the [middlewares/psr15-middlewares](https://github.com/middlewares/psr15-middlewares) repository.

