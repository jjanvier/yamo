<?php

namespace Jjanvier\Yamo;

use Http\Factory\Diactoros\ResponseFactory;
use Interop\Http\Factory\ResponseFactoryInterface;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;

class Orchestrator implements DelegateInterface
{
    /** @var ResponseFactoryInterface */
    private $responseFactory;

    /** @var MiddlewareInterface[] */
    private $middlewares;

    /** @var int */
    private $currentIndex;

    /**
     * @param MiddlewareInterface[]    $middlewares
     * @param ResponseFactoryInterface $responseFactory
     */
    public function __construct(array $middlewares, ResponseFactoryInterface $responseFactory = null)
    {
        if (0 === count($middlewares)) {
            throw new \InvalidArgumentException('At least one middleware is expected.');
        }

        foreach ($middlewares as $middleware) {
            if (!$middleware instanceof MiddlewareInterface) {
                throw new \InvalidArgumentException(sprintf('Middleware must implement "%s".', MiddlewareInterface::class));
            }
        }

        if (null === $responseFactory) {
            $responseFactory = new ResponseFactory();
        }

        $this->responseFactory = $responseFactory;
        $this->currentIndex = 0;
        $this->middlewares = $middlewares;
    }

    /**
     * {@inheritdoc}
     */
    public function process(ServerRequestInterface $request)
    {
        $response = null;
        $middleware = $this->getCurrentMiddlewareToUse($this->currentIndex);
        if (null !== $middleware) {
            $this->currentIndex++;
            $response = $middleware->process($request, $this);
        }

        if (null === $response) {
            $response = $this->responseFactory->createResponse();
        }

        return $response;
    }
    
    /**
     * @param int $index
     *
     * @return MiddlewareInterface|null
     */
    private function getCurrentMiddlewareToUse(int $index)
    {
        if (isset($this->middlewares[$index])) {
            return $this->middlewares[$index];
        }

        return null;
    }
}
